parameter Voltage VaNominal = 12 "Nominal armature voltage";
parameter Current IaNominal = 12.5 "Nominal armature current";
parameter AngularVelocity wNominal = Modelica.SIunits.Conversions.from_rpm(2500)
"Nominal angular velocity";
parameter Resistance Ra = 72E-3 "Armature resistance";
parameter Inductance La = 7.2E-3 "Armature inductance";
parameter Inertia J = 1E-3 "Machine inertial";
parameter Real ratio = 76.92 "Total gear and chain ratio";
parameter Radius radius = 0.7112 / 2 "Wheel radius";
