# LyX

Auf Deutsch gibt es folgende offizelle Dokumente:

- [Tutorium](http://www.ftp.lyx.de/Documentation/de/Tutorial.pdf)
- [Benutzerhandbuch](http://www.ftp.lyx.de/Documentation/de/UserGuide.pdf)
- [Das LyX-Handbuch der Einfügungen](http://www.ftp.lyx.de/Documentation/de/EmbeddedObjects.pdf)
- [LyX’ detailliertes Mathe Handbuch](http://www.ftp.lyx.de/Documentation/de/Math.pdf)

# LyX Vorlagen am TGM

LyX Schüler-Vorlagen für die Höhere Abteilung für Elektrotechnik (HET) des [TGM](https://www.tgm.ac.at)

## `TGM_Diplomarbeit`

- Alle Dateien und Verzeichnisse aus `TGM_Diplomarbeit` müssen in ein lokales Verzeichnis kopiert werden
- Die Unterverzeichnisse dürfen nicht verschoben werden
- Das Hauptdokument ist `TGM_Diplomarbeit.lyx`
- Zusätzlich zum Hauptdokument gibt es in der Vorlage vier (schülerspezifische) Unterdokumente
    - `TGM_01Einleitung.lyx`
    - `TGM_02.lyx`
    - `TGM_03.lyx`
    - `TGM_AnhangA.lyx`
- Die Datei `TGM_DA_Template.lyx` ist eine Minimal-Vorlage für ein weiteres Kapitel oder Unterverzeichnis

## `TGM_Handout.lyx`

- Einfaches Handout mit TGM-Logo ab der zweiten Seite, falls eine Titelseite erzeugt wird; ohne Titelseite wird das TGM-Logo bereits auf der ersten Seite angezeigt

## `TGM_Lab_Report.lyx`

- Laborprotokoll Engisch

## `TGM_Laborprotokoll.lyx`

- Laborprotokoll Deutsch

## `TGM_Unterlagen.lyx`

- Kleines Skriptum mit grau unterlegtem Titel
